
provider "aws" {
    # monofuel34089+aws_training@gmail.com user
    profile = "aws_training"
	region = var.aws_region
}

provider "aws" {
    # monofuel34089+aws_training@gmail.com user
	alias = "eu-west-1"
    profile = "aws_training"
	region = "eu-west-1"
}

terraform {
	backend "s3" {
		bucket = "mono-training-terraform"
		key = "mono-training/tf.state"
		region = "us-east-1"
	}
}

resource "aws_s3_bucket" "b" {
	bucket = "mono-training-terraform"
 	acl    = "private"
	tags = {
		Managed = "terraform"
	}
}

resource "aws_iam_user" "terraform" {
	name = "terraform"
	tags = {
		Managed = "terraform"
	}
}

resource "aws_iam_user_policy" "terraform" {
  user = aws_iam_user.terraform.name

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "*",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_user" "abrower" {
	name = "andrew.brower"
	tags = {
		Managed = "terraform"
	}
}

resource "aws_iam_group" "developers" {
	name = "Developers"
}

resource "aws_iam_role" "S3admin" {
	name = "S3_admin_access"
	description = "Allows S3 to call AWS services on your behalf."
	assume_role_policy = <<EOF
{
	"Version": "2012-10-17",
	"Statement": [
		{
			"Action": "sts:AssumeRole",
			"Effect": "Allow",
			"Principal": {
				"Service": "s3.amazonaws.com"
			}
		}
	]
}
EOF
	tags = {
		Managed = "terraform"
	}
}

resource "aws_cloudwatch_metric_alarm" "billing_alarm" {
	alarm_name = "Billing Alarm"
	alarm_actions             = [
        "arn:aws:sns:us-east-1:581991185755:Billing_Alarm",
    ]
    alarm_description         = "send me an email if my bill goes over $10"
    comparison_operator       = "GreaterThanThreshold"
	dimensions                = {
        "Currency" = "USD"
    }
	evaluation_periods        = 1
	metric_name               = "EstimatedCharges"
    namespace                 = "AWS/Billing"
	period                    = 21600
    statistic                 = "Maximum"
    threshold                 = 10
	datapoints_to_alarm       = 1
	tags = {
		Managed = "terraform"
	}
}

resource "aws_sns_topic" "billing_alarm" {
	name = "Billing_Alarm"
	tags = {
		Managed = "terraform"
	}
}

# NB email is not supported by terraform
# resource "aws_sns_topic_subscription" "billing_email" {
# 	topic_arn = aws_sns_topic.billing_alarm.arn
# 	endpoint             = "monofuel34089+aws_training@gmail.com"
# 	protocol             = "email"
# 	raw_message_delivery = false
# }



resource "aws_s3_bucket" "monofuel" {
	bucket = "monofuel-123456"
 	acl    = "private"
	tags = {
		Managed = "terraform"
	}
}
