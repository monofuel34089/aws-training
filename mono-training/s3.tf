# S3 training resources

resource "aws_iam_role" "replication" {
  name = "tf-iam-role-replication-12345"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "s3.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}

resource "aws_iam_policy" "replication" {
  name = "tf-iam-role-policy-replication-12345"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:GetReplicationConfiguration",
        "s3:ListBucket"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_s3_bucket.versioning.arn}"
      ]
    },
    {
      "Action": [
        "s3:GetObjectVersion",
        "s3:GetObjectVersionAcl"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_s3_bucket.versioning.arn}/*"
      ]
    },
    {
      "Action": [
        "s3:ReplicateObject",
        "s3:ReplicateDelete"
      ],
      "Effect": "Allow",
      "Resource": "${aws_s3_bucket.destination.arn}/*"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "replication" {
  role       = aws_iam_role.replication.name
  policy_arn = aws_iam_policy.replication.arn
}

resource "aws_s3_bucket" "destination" {
	bucket = "monofuel-destination"
 	acl    = "private"
	tags = {
		Managed = "terraform"
	}
	versioning {
	  enabled = true
	}
}

resource "aws_s3_bucket" "versioning" {

    # Doh, should have had the regions for this and destination swapped around

    provider = aws.eu-west-1
    bucket = "monofuel-versioning-12345"
    region = "eu-west-1"
    versioning {
    enabled = true
    }

    replication_configuration {
    role = "${aws_iam_role.replication.arn}"

    rules {
        id     = "replrule"
        status = "Enabled"

        destination {
        bucket        = "${aws_s3_bucket.destination.arn}"
        storage_class = "STANDARD"
        }
    }
}
}